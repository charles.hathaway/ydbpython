import unittest
from yottadb import *

class TestSimpleApiMethods(unittest.TestCase):
    def get_buffers(self, sub_values=["subA", "subB"]):
        return Buffer("^ydbPython"), BufferArray(values=sub_values), Buffer(length=1024)
    def test_data_st(self):
        gbl, subs, err = self.get_buffers(sub_values=["doesNotExist"])
        tptoken = 0
        out = ffi.new("unsigned int *")
        status = data(tptoken, err.buf, gbl.buf, 1, subs.buf, out)
        if status != 0:
            raise str(err.val())
        self.assertEqual(out[0], 0)

    def test_sub_next(self):
        # Set a few globals
        gbl, subs, err = self.get_buffers()
        tptoken = 0
        val = Buffer("hello!")
        status = set(tptoken, err.buf, gbl.buf, 1, subs.buf, val.buf)
        if status != 0:
            raise str(err.val())
        subs[0].set_val("subB")
        status = set(tptoken, err.buf, gbl.buf, 1, subs.buf, val.buf)
        if status != 0:
            raise str(err.val())
        subs[0].set_val("")
        # Fetch the next node
        status = sub_next(tptoken, err.buf, gbl.buf, 1, subs.buf, subs[0].buf)
        if status != 0:
            raise str(err.val())
        self.assertEqual(b'subA', subs[0].val())
        status = sub_next(tptoken, err.buf, gbl.buf, 1, subs.buf, subs[0].buf)
        if status != 0:
            raise str(err.val())
        self.assertEqual(b'subB', subs[0].val())

    def test_key(self):
        ctx = Context()
        k = ctx.key("^ydbPython")["subA"]
        res = k.get()
        self.assertEqual(b'hello!', res)
        k2 = ctx.key("^ydbPython")["subA"]["subB"]
        exception_caught = False
        try:
            res = k2.get()
        except:
            exception_caught = True
        self.assertTrue(exception_caught)
        k3 = ctx.key("^ydbPython")
        exception_caught = False
        try:
            res = k3.get()
        except:
            exception_caught = True
        self.assertTrue(exception_caught)

        k.delete(YDB_DEL_NODE)

        k.set('hello!')

    def test_shortcut_create(self):
        ctx = Context()
        key = ctx.key('^ydbPython', 'subA')
        res = key.get()
        self.assertEqual(b'hello!', res)

    def test_forward_sub_iterator(self):
        ctx = Context()
        key = ctx.key('^ydbPython', 'testForwardSubIterator')
        key['A'].set('hello')
        key['B'].set('hello')
        key['C'].set('hello')
        key['D'].set('hello')
        expected = [b'A', b'B', b'C', b'D']
        for i, v in enumerate(key):
            self.assertEqual(expected[i], v.val())

    def test_reverse_sub_iterator(self):
        ctx = Context()
        key = ctx.key('^ydbPython', 'testForwardSubIterator')
        key['A'].set('hello')
        key['B'].set('hello')
        key['C'].set('hello')
        key['D'].set('hello')
        expected = [i for i in reversed([b'A', b'B', b'C', b'D'])]
        for i, v in enumerate(reversed(key)):
            self.assertEqual(expected[i], v.val())

    def test_increment(self):
        ctx = Context()
        ctx.key('^ydbPython', 'test_increment').delete()
        value = ctx.key('^ydbPython', 'test_increment').incr()
        self.assertEqual(value, b'1')
